<?php

/**
 * 
 *
 */
abstract class MigrateDataTestCase extends DrupalWebTestCase {
    
  protected $SAMPLE_SIZE = 2500; // 2500 should cover all migrated nodes
  private $DIRECTORY = 'public://migration_test_results/'; // Not currently used
  private $FILE_NAME = 'migration_test_results/results'; // Not currently used
  
  public $errors = array();
  
  /**
   * By default SimpleTest will create a sandbox database and run all the tests
   * in there. This override to the setUp function ensures that we use the real
   * database instead where the migrated data is accessible.
   *
   * http://lucor.github.com/2012/04/drupal-speed-up-simpletest-tests-usign-live-database
   */
  // TODO: Change this class to extend DrupalCloneTestCase
  function setUp() {
    $this->setup = TRUE;
  }
  
  /**
   * Overrides the tearDown method so that the database tables are NOT removed.
   *
   * We are using the real database so we need to be VERY careful to leave it as
   * we found it.
   *
   */
  function tearDown() {
  }
  
  /**
   * Returns array keyed with source entity ids and values of destination ids
   *
   * @param $classname
   *   The name of the class to be tested as it appears on the migrate interface
   *   
   * @return An array containing the source and destination ids in the form
   *   array(
   *     [sourceid] => [destinationid],
   *     753 => 535
   *   ); 
   */
  protected function getSourceToDestinationIds($class) {
    // Make the migration class all lower case and remove 'migration' from the
    // end to get the table name
    // TODO: This needs to be tightened up incase there is the word migration
    // more than once in the class name
    $class = str_replace('migration', NULL, strtolower($class));
    
    $SourceToDestinationIds = array();
    $table = 'migrate_map_'.$class;
    //print($table."\n\n");
    // Check if the classname (i.e table) exists and has migrated content
    if(!db_table_exists($table)) {
      return NULL;
    }
    
    $query = db_select($table, 'a')
      ->fields('a', array('sourceid1', 'destid1'))
      ->orderRandom() // Sort randomly before selecting the range
      ->range(0, $this->SAMPLE_SIZE); // Limit to a specified number of records
    
    $result = $query->execute(); 
    // Get the source ids and destination ids and insert in an array
    while($records = $result->fetchAssoc()) {
      $SourceToDestinationIds[$records['sourceid1']] = $records['destid1'];          
    }
    return $SourceToDestinationIds;
  }
  
  /**
   * Returns destination nodes given an id
   * 
   * @param $destination_id
   *
   * @return The node object provided by node_load
   *
   * TODO: Expand this so that it can load entities of different types
   */
  protected function getDestinationEntity($destination_id) {
    $node = node_load($destination_id);
    if (!$node) {
      //throw new MigrationTestFailureException('No node found with nid '. $destination_id . '.');
    }
    return $node;
  } 

  
  /**
   * Handles the array of report messages once the tests have run.
   *
   * @param $errors
   *   An array of errors in text format
   * @param $class
   *   The name of the class that the tests were run on
   *
   */
  protected function writeReport($reports = array(), $class = NULL) {
    global $base_url;
    // Create a new CSV file for us to record the reports
    // Ensure the path here is writeable
    $file = fopen('private://'.strtolower($class).'-report.csv', 'w');
    //print realpath('public://'.strtolower($class).'-report.csv');
    // Set an array of headers for the CSV file.
    $headers = array ('class', 'callback', 'source id', 'destination id', 'source field',
      'destination field', 'source value', 'destination value');
    $int = fputcsv($file, $headers, ',','"');
   
    foreach ($reports as $report) {
      // Write this report line to the CSV file
      $int = fputcsv($file, $report, ',','"');
    }
  }
  
  /**
   * Sets up some variables we'll use in this class
   *
   */
  function __construct($test_id = NULL) {
    parent::__construct($test_id);
    $this->testsToRun = array();
    // TODO: Could use getMappings and getTargetClass here and add them as
    // properties of this class
  }
  
  /**
   * Gets the name of the migrate class who's data we want to test
   *
   * @return The name of the migrate class
   */
  protected function getTargetClass() {
    $class = str_replace('TestCase', NULL, get_called_class());
    return $class;
  }
  
  /**
   * Loads the mapping information for a migrate class
   *
   * @return An array of migrate mappings keyed by the destination fields of
   * each one.
   */
  protected function getMappings($class) {
    $MigrateClass = new $class;
    return $MigrateClass->getFieldMappings();
  }
  
  /**
   * Returns the source fields to test against.
   *
   * @return An an indexed array of stdClass objects each containing a row in
   * the source database table
   */
  protected function getSourceFields($class) {
    $MigrateClass = new $class;
    // TODO: We need a more efficient and portable way of doing this. Is it
    // possible to get the source query results from the migrate object itself?
    $results = Database::getConnection('default', 'source')
      // TODO: These properties should not be hard coded.
      ->select('urls', 'u')
      ->fields('u', array('id', 'title', 'url'))
      // We need to impose a limit here. Maybe just get the source ids in the
      // migrate_map_ table?
      ->execute();
    return $results->fetchAllAssoc('id');
  }
  
  /**
   * Adds a test that will be run on a migrated field
   *
   * @param $destination
   *   The machine name of the destination field
   * @param $source
   *   The name of the source column in the source database
   */
  protected function addFieldTest($destination, $source = NULL) {
    // Create a new test array and add the destination field name to it
    $test = array(
      'destination' => $destination,
    );
    
    // See if a source field was passed. If not, try and find out what it is
    if (empty($source)) {
      // The class who's mappings we will get is assumed to be named similarly to
      // the implementation of this class. E.g. to test a migrate class called
      // BlogBaseMigration, we'd create a class that extends this one called
      // BlogBaseMigrationTestCase.
      $class = $this->getTargetClass();
      $mappings = $this->getMappings($class);
      $source = $mappings['title']->getSourceField();
    }
    
    // Check that a source field is set
    if (empty($source)) {
      exit;
    }
    
    $test['source'] = $source;
    
    // Add to the array of tests
    $this->testsToRun[] = $test;
    return $this;
  }
  
  /**
   * Adds a callback function that will receive a source and destination field
   *
   * @param $function
   *   An array containing names of all the callback functions to be executed
   */
  protected function callbacks($callbacks = array()) {
    // Get the last element of the array of tests to run
    $test = array_pop($this->testsToRun);
    // Add all the callbacks to it
    $test['callbacks'] = $callbacks;
    // Put it back on the array
    array_push($this->testsToRun, $test);
    return $this;
  }

  /**
   * Runs tests defined by the test functions in the custom migrate test module
   *
   */
  protected function executeTests() {
    // Get the name of the class we're testing
    $class = $this->getTargetClass();
    // Load an array of the source and destination ids from the database.
    $ids = $this->getSourceToDestinationIds($class);
    $sourcefields = $this->getSourceFields($class);
    // Initiate an array to hold the test report
    $report = array();
    
    foreach($ids as $sourceid => $destid) {
      $source = $sourcefields[$sourceid];
      $destination = $this->getDestinationEntity($destid);

      // Loop through each test that has been defined in the custom test class
      foreach($this->testsToRun as $test) {
        // Each test may have one or more callbacks. Loop through them and pass
        // the source and destination fields they are expecting.
        foreach($test['callbacks'] as $callback) {
          $result = $this->$callback($source->$test['source'], $destination->$test['destination']);
          // The failed result needs to be added to an array keyed with the name
          // of the test we are running and some details of the results
          if (!$result) {
            $report[] = array (
              'class' => $class, // The class we're testing
              'callback' => $callback, // The name of the callback that failed
              'source' => $sourceid, // The id of the source row
              'destination' => $destid, // The id of the destination entity
              'source field' => $test['source'],
              'destination field' => $test['destination'],
              'source value' => $source->$test['source'],
              'destination value' => $destination->{$test['destination']}[LANGUAGE_NONE][0]['value'],
            );
          }
        }
      }
    }
    $this->writeReport($report, $class);
  }
}
